@extends('layouts.app')

@section('content')
    <div class="container">
        <div class="row">
            <div class="col-md-8 col-md-offset-2">
                <div class="form-group">
                    <label for="name">Name:</label>
                    <input type="text" class="form-control" name="name" value="{{$product->name}}" disabled>
                </div>
                <div class="form-group">
                    <label for="price">Price:</label>
                    <input type="number" class="form-control" name="price" value="{{$product->price}}" disabled>
                </div>
                <div class="form-group">
                    <label for="brand">Brand:</label>
                    <input type="text" class="form-control" name="brand" value="{{$product->brand}}" disabled>
                </div>
                <div class="form-group">
                    <label for="stock">Stock:</label>
                    <input type="number" class="form-control" name="stock" value="{{$product->stock}}" disabled>
                </div>
                <div class="form-group">
                    <label>ImageUrl:</label>
                    <br>
                    <img src="{{$product->imageUrl}}" alt="" width="50%">
                </div>
                <hr>
                <div class="form-group">
                    <a href="/products" class="btn btn-danger">Cancel</a>
                </div>
            </div>
        </div>
    </div>
@endsection
