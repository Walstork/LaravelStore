@extends('layouts.app')

@section('content')
    <div class="container">
        <div class="row">
            <div class="col-md-8 col-md-offset-2">
                <form action="/api/products/{{$product->id}}" method="POST">
                    {{csrf_field()}}
                    {{method_field('PUT')}}
                    <div class="form-group">
                        <label for="type">Name:</label>
                        <input type="text" class="form-control" name="name" value="{{$product->name}}" required>
                    </div>
                    <div class="form-group">
                        <label for="type">Price:</label>
                        <input type="number" class="form-control" name="price" value="{{$product->price}}" required>
                    </div>
                    <div class="form-group">
                        <label for="type">Brand:</label>
                        <input type="text" class="form-control" name="brand" value="{{$product->brand}}" required>
                    </div>
                    <div class="form-group">
                        <label for="type">Stock:</label>
                        <input type="number" class="form-control" name="stock" value="{{$product->stock}}" required>
                    </div>
                    <div class="form-group">
                        <label for="type">ImageUrl:</label>
                        <input type="text" class="form-control" name="imageUrl" value="{{$product->imageUrl}}" required>
                    </div>
                    <hr>
                    <div class="form-group">
                        <button type="submit" class="btn btn-success">Update</button>
                        <a href="/products" class="btn btn-danger">Cancel</a>
                    </div>
                </form>
            </div>
        </div>
    </div>
@endsection
