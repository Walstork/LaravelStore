@extends('layouts.app')

@section('content')
    <div class="container">
        <div class="row">
            <div class="col-md-8 col-md-offset-2">
                <form action="/api/products" method="post" enctype="multipart/form-data">
                    {{csrf_field()}}
                    <div class="form-group">
                        <label for="type">Name:</label>
                        <input type="text" class="form-control" name="name" required>
                    </div>
                    <div class="form-group">
                        <label for="type">Price:</label>
                        <input type="number" class="form-control" name="price" required>
                    </div>
                    <div class="form-group">
                        <label for="type">Brand:</label>
                        <input type="text" class="form-control" name="brand" required>
                    </div>
                    <div class="form-group">
                        <label for="type">Stock:</label>
                        <input type="number" class="form-control" name="stock" required>
                    </div>
                    <!--<div class="form-group">
                        <label for="type">ImageUrl:</label>
                        <input type="text" class="form-control" name="imageUrl" required>
                    </div>-->
                    <div class="form-group">
                        <label for="type">Img:</label>
                        <input type="file" class="form-control" name="image" required>
                    </div>
                    <hr>
                    <div class="form-group">
                        <button type="submit" class="btn btn-success">Create</button>
                        <a href="{{ route('products.index') }}" class="btn btn-danger">Cancel</a>
                    </div>
                </form>
            </div>
        </div>
    </div>
@endsection
