<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Product extends Model
{
    protected $fillable = ['name', 'price', 'brand', 'stock', 'imageUrl', 'created_at', 'updated_at'];
}
